## Puppet and Terraform Integration

This repository contains the code and instructions for automating configuration management using Puppet and Terraform.

### Objective:

The objective of this assignment is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.

### Prerequisites:

- Terraform installed on your local machine.
- Access to an AWS account with appropriate permissions to create EC2 instances.
- SSH key pair generated for accessing EC2 instances.
- Ensure that your AWS credentials are configured either through environment variables or AWS CLI configuration.

## Infrastructure Provisioning with Terraform

### Terraform Configuration:
1. Terraform code for provisioning infrastructure components is written in the main.tf file.
2. This code defines the AWS EC2 instance to be provisioned.

### Terraform Tasks:
1. Terraform provisions the infrastructure using the specified configuration.
2. Once provisioned, Terraform triggers Puppet configuration management to apply desired configurations.

## Configuration Management with Puppet
### Puppet Manifests:
1. Puppet manifests are written in the puppet directory.
2. These manifests define the configuration tasks for provisioning infrastructure components.

### Puppet Tasks:
The Puppet manifests include tasks such as installing required packages or software, configuring system settings or files, and starting or enabling services.

## Integration
1. Triggering Puppet with Terraform Provisioners
2. Terraform provisioners are used to trigger Puppet configuration management after infrastructure provisioning.
3. The remote-exec provisioner is utilized to execute Puppet commands on provisioned instances.

### Executing:
1. Run terraform init to initialize the Terraform project.
2. Run terraform validate to validate the code.
3. Run terraform plan to review the execution plan.
4. Run terraform apply to apply the Terraform configuration and provision the infrastructure.

## Validate Integration
1. Test the integration by provisioning infrastructure using Terraform.
2. Ensure that Puppet successfully configures the provisioned resources according to the defined manifests.
