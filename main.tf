provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "terraform_demo"{
    ami="ami-0440d3b780d96b29d"
    instance_type="t2.micro"
    key_name = "tf_demo_key"

    tags={
      Name="terraform_demo"
    }

    connection {
      type = "ssh"
      user = "Administrator"
      private_key = file("Gitlab_Server_keypair.pem")
      host        = self.public_ip
      timeout     = "5m"

    }

    provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt-get update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"192.168.0.107 puppet\" >> /etc/hosts'",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test",
      "sudo puppet apply C:\\Users\\91915\\Documents\\Worldline task\\task4\\puppet\\webserver.pp",  # Apply Puppet manifests

    ]
  }
}


output "public_ip" {
  value = aws_instance.terraform_demo.public_ip
}


